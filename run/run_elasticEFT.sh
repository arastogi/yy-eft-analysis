listEFTlin=("mc16_13TeV.506608.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FM0.merge.EVNT.e8279_e7400"
	    "mc16_13TeV.506610.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FM1.merge.EVNT.e8279_e7400"
	    "mc16_13TeV.506612.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FM2.merge.EVNT.e8279_e7400"
	    "mc16_13TeV.506614.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FM3.merge.EVNT.e8279_e7400"
	    "mc16_13TeV.506616.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FM4.merge.EVNT.e8279_e7400"
	    "mc16_13TeV.506618.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FM5.merge.EVNT.e8279_e7400"
	    "mc16_13TeV.506620.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FM7.merge.EVNT.e8279_e7400"
	    "mc16_13TeV.506622.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FT0.merge.EVNT.e8279_e7400"
	    "mc16_13TeV.506624.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FT1.merge.EVNT.e8279_e7400"
	    "mc16_13TeV.506626.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FT2.merge.EVNT.e8279_e7400"
	    "mc16_13TeV.506628.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FT5.merge.EVNT.e8279_e7400"
	    "mc16_13TeV.506630.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FT6.merge.EVNT.e8279_e7400"
	    "mc16_13TeV.506632.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FT7.merge.EVNT.e8279_e7400"
	   )

listEFTquad=("mc16_13TeV.506609.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FM0quad.merge.EVNT.e8279_e7400"
	     "mc16_13TeV.506611.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FM1quad.merge.EVNT.e8279_e7400"
	     "mc16_13TeV.506613.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FM2quad.merge.EVNT.e8279_e7400"
	     "mc16_13TeV.506615.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FM3quad.merge.EVNT.e8279_e7400"
	     "mc16_13TeV.506617.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FM4quad.merge.EVNT.e8279_e7400"
	     "mc16_13TeV.506619.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FM5quad.merge.EVNT.e8279_e7400"
	     "mc16_13TeV.506621.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FM7quad.merge.EVNT.e8279_e7400"
	     "mc16_13TeV.506623.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FT0quad.merge.EVNT.e8279_e7400"
	     "mc16_13TeV.506625.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FT1quad.merge.EVNT.e8279_e7400"
	     "mc16_13TeV.506627.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FT2quad.merge.EVNT.e8279_e7400"
	     "mc16_13TeV.506629.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FT5quad.merge.EVNT.e8279_e7400"
	     "mc16_13TeV.506631.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FT6quad.merge.EVNT.e8279_e7400"
	     "mc16_13TeV.506633.MGPy8EG_CT14qed_yyWW_dim8_Ind5v2020v2_FT7quad.merge.EVNT.e8279_e7400"
	    )

EFTopr=("M0" "M1" "M2" "M3" "M4" "M5" "M7" "T0" "T1" "T2" "T5" "T6" "T7")

for i in ${!EFTopr[@]}
do
    indirname="/global/homes/a/arastogi/yyWW_MCGeneration/mcgeneration-yy-ww/run/MCgen/ElasticEFT/"${listEFTlin[$i]}
    echo "input directory = "$indirname
    outdirname="Run_"${EFTopr[$i]}"linear_Elastic"
    MCVal_eljob.py -s $outdirname -i $indirname
done

for i in ${!EFTopr[@]}
do
    indirname="/global/homes/a/arastogi/yyWW_MCGeneration/mcgeneration-yy-ww/run/MCgen/ElasticEFT/"${listEFTquad[$i]}
    echo "input directory = "$indirname
    outdirname="Run_"${EFTopr[$i]}"quad_Elastic"
    MCVal_eljob.py -s $outdirname -i $indirname
done

