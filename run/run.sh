EFTopr=("M0" "M1" "M2" "M3" "M4" "M5" "M7" "T0" "T1" "T2" "T5" "T6" "T7")
dirpfx=("87" "88" "89" "90" "91" "92" "93" "94" "95" "96" "97" "98" "99")                                                                                               
component=("00" "01" "02")                                                                                                                                           
comp_label=("SD" "DS" "DD")                                                                                                                                          
variations=("00" "01" "02" "03" "04" "05" "06" "07" "08" "09")  

for i in ${!EFTopr[@]}
do
    for j in ${!component[@]}
    do
        for k in ${!variations[@]}
        do
        filein="DAOD_TRUTH0.user.arastogi.mc.MGPy8EG_CT14qed_yyWW_"${comp_label[$j]}"_dim8_Ind5v2020v2_"${EFTopr[$i]}"linear_Gen_"${dirpfx[$i]}${component[$j]}${variations[$k]}".EVNT.e8380.root"
        echo "input file = "$filein
	indirname="/global/homes/a/arastogi/yyWW_MCGeneration/mcgeneration-yy-ww/run/MCgen/TruthDAOD_"${EFTopr[$i]}"linear"
	outdirname="Run_"${EFTopr[$i]}"linear_"${comp_label[$j]}${variations[$k]}
	rm -rf outdirname
	MCVal_eljob.py -s $outdirname -i $indirname -f $filein
	done
    done
done

component=("10") # "11" "12")

for i in ${!EFTopr[@]}
do
    for j in ${!component[@]}
    do
        for k in ${!variations[@]}
        do
        filein="DAOD_TRUTH0.user.arastogi.mc.MGPy8EG_CT14qed_yyWW_"${comp_label[$j]}"_dim8_Ind5v2020v2_"${EFTopr[$i]}"quad_Gen_"${dirpfx[$i]}${component[$j]}${variations[$k]}".EVNT.e8380.root"
        echo "input file = "$filein
        indirname="/global/homes/a/arastogi/yyWW_MCGeneration/mcgeneration-yy-ww/run/MCgen/TruthDAOD_"${EFTopr[$i]}"quad"
        outdirname="Run_"${EFTopr[$i]}"quad_"${comp_label[$j]}${variations[$k]}
	rm -rf outdirname
        MCVal_eljob.py -s $outdirname -i $indirname -f $filein
	done
    done
done

