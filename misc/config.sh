export PS1="\u@\H:\W> "
if ! [ -z "${SHIFTER_IMAGEREQUEST}" ]; then
  imageStr=`echo ${SHIFTER_IMAGEREQUEST} | rev | cut -d '/' -f 1 | rev`
  export PS1="${imageStr}:\W> "
fi

alias ls='ls -ltrh --color=tty'

lsetup git
cd build
asetup AnalysisBase,22.2.89
source x86_64-*/setup.sh
cd ..
